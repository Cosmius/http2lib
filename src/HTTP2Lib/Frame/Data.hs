{-# LANGUAGE RecordWildCards #-}
module HTTP2Lib.Frame.Data
  ( decodeDataPayload
  ) where

import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.ByteString

import           HTTP2Lib.Frame.Internal

{-|
DATA Frame Payload

@
 +---------------+
 |Pad Length? (8)|
 +---------------+-----------------------------------------------+
 |                            Data (*)                         ...
 +---------------------------------------------------------------+
 |                           Padding (*)                       ...
 +---------------------------------------------------------------+
@
-}

decodeDataPayload :: FrameHeader -> Parser ByteString
decodeDataPayload FrameHeader{..} = do
  let hasPadding = frameFlags `hasFlag` padded
  padLen <- if hasPadding then fromIntegral <$> P.anyWord8 else return 0
  let fragLen = frameLen - (if hasPadding then padLen + 1 else 0)
  dataFrag <- P.take fragLen
  _ <- P.take padLen
  return dataFrag
