{-# LANGUAGE RecordWildCards #-}
module HTTP2Lib.Frame.Priority
  ( PriorityData(..)
  , decodePriorityPayload
  , decodePriorityData
  ) where

import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.Bits
import           Data.Word

import           HTTP2Lib.Utils
import           HTTP2Lib.Frame.Internal

{-|
PRIORITY Frame Payload

@
 +-+-------------------------------------------------------------+
 |E|                  Stream Dependency (31)                     |
 +-+-------------+-----------------------------------------------+
 |   Weight (8)  |
 +-+-------------+
@
-}

data PriorityData = PriorityData
  { priorityIsExclusive :: Bool
  , priorityStreamDep   :: Word32
  , priorityWeight      :: Word8
  }
  deriving (Show)

decodePriorityPayload :: FrameHeader -> Parser PriorityData
decodePriorityPayload FrameHeader{..} =
  assert (frameLen == 5) "priority frame length must be 5"
  >> decodePriorityData

decodePriorityData :: Parser PriorityData
decodePriorityData = do
  sd <- pWord32BE
  priorityWeight <- P.anyWord8
  let priorityIsExclusive = testBit sd 31
      priorityStreamDep   = clearBit sd 31
  return PriorityData{..}
