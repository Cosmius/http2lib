module HTTP2Lib.Frame.Unknown
  ( UnknownPayload(..)
  , decodeUnknownPayload
  ) where

import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.ByteString
import           Data.Word

import           HTTP2Lib.Frame.Internal

data UnknownPayload = UnknownPayload
  { fType    :: Word8
  , fPayload :: ByteString
  }
  deriving (Show)

decodeUnknownPayload :: Word8 -> FrameHeader -> Parser UnknownPayload
decodeUnknownPayload ft FrameHeader{frameLen=len} =
  UnknownPayload ft <$> P.take len
