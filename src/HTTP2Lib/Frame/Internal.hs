module HTTP2Lib.Frame.Internal
  ( FrameHeader(..)
  , hasFlag, endStream, endHeaders, padded, priority, ack
  ) where

import           Data.Bits
import           Data.Word

-- * Frame Header

{-|

The frame structure.

@
 +-----------------------------------------------+
 |                 Length (24)                   |
 +---------------+---------------+---------------+
 |   Type (8)    |   Flags (8)   |
 +-+-------------+---------------+-------------------------------+
 |R|                 Stream Identifier (31)                      |
 +=+=============================================================+
 |                   Frame Payload (0...)                      ...
 +---------------------------------------------------------------+
@

The header type does not include a type, since payload itself is sufficient.
-}
data FrameHeader = FrameHeader
  { frameLen      :: Int
  , frameFlags    :: Word8
  , frameStreamId :: Word32
  }
  deriving (Show)

-- * Flags

hasFlag :: Bits a => a -> a -> Bool
hasFlag flags flag = flags .&. flag /= zeroBits

endStream :: Word8
endStream = 0x1

endHeaders :: Word8
endHeaders = 0x4

padded :: Word8
padded = 0x8

priority :: Word8
priority = 0x20

ack :: Word8
ack = 0x1
