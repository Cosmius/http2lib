{-# LANGUAGE RecordWildCards #-}
module HTTP2Lib.Frame.Headers
  ( HeadersPayload(..)
  , decodeHeadersPayload
  ) where

import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.ByteString

import           HTTP2Lib.Frame.Priority
import           HTTP2Lib.Frame.Internal

{-|
HEADERS Frame Payload

@
 +---------------+
 |Pad Length? (8)|
 +-+-------------+-----------------------------------------------+
 |E|                 Stream Dependency? (31)                     |
 +-+-------------+-----------------------------------------------+
 |  Weight? (8)  |
 +-+-------------+-----------------------------------------------+
 |                   Header Block Fragment (*)                 ...
 +---------------------------------------------------------------+
 |                           Padding (*)                       ...
 +---------------------------------------------------------------+
@
-}
data HeadersPayload = HeadersPayload
  { headersPriority :: Maybe PriorityData
  , headersFrag     :: ByteString
  }
  deriving (Show)

decodeHeadersPayload :: FrameHeader -> Parser HeadersPayload
decodeHeadersPayload FrameHeader{..} = do
  let hasPadding  = frameFlags `hasFlag` padded
      hasPriority = frameFlags `hasFlag` priority
  padLen <- if hasPadding then fromIntegral <$> P.anyWord8 else return 0
  headersPriority <- if hasPriority
    then Just <$> decodePriorityData
    else return Nothing
  let fragLen = frameLen
        - (if hasPadding then padLen + 1 else 0)
        - (if hasPriority then 5 else 0)
  headersFrag <- P.take fragLen
  _ <- P.take padLen
  return HeadersPayload{..}
