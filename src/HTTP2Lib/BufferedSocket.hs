{-# LANGUAGE RecordWildCards #-}
module HTTP2Lib.BufferedSocket
  ( BufferedSocket, BufferdIO
  , fromSocket, new, close, connect
  , recvS, recvL, withBufS, withBufL, sendLAll, sendSAll
  ) where

import           Control.Arrow
import           Control.Concurrent.MVar
import qualified Data.ByteString                as BS
import qualified Data.ByteString.Lazy           as BL
import           Data.Semigroup
import           Data.Tuple
import           Network.Socket                 (AddrInfo (..), Family (..),
                                                 Socket, SocketType (..))
import qualified Network.Socket                 as S
import qualified Network.Socket.ByteString      as SS
import qualified Network.Socket.ByteString.Lazy as SL

data BufferedSocket = BufferedSocket
  { bSocket :: Socket             -- ^ Underlying socket
  , bBuffer :: MVar BS.ByteString -- ^ Buffer
  }

fromSocket :: Socket -> IO BufferedSocket
fromSocket s = BufferedSocket s <$> newMVar BS.empty

new :: IO BufferedSocket
new = S.socket AF_INET Stream S.defaultProtocol >>= fromSocket

close :: BufferedSocket -> IO ()
close = S.close . bSocket

connect :: BufferedSocket -> String -> String -> IO ()
connect BufferedSocket{bSocket=s} host port = do
  addr' <- S.getAddrInfo Nothing (Just host) (Just port)
  addr <- case addr' of
    ai:_ -> return (addrAddress ai)
    _    -> error $ "Unable to resolve " <> host <> ":" <> port
  S.connect s addr

recvS :: BufferedSocket -> Int -> IO BS.ByteString
recvS BufferedSocket{..} n = modifyMVar bBuffer $ \buf -> do
  let bufLen = BS.length buf
  if n <= bufLen
    then return . swap . BS.splitAt n $ buf
    else do
      b <- SS.recv bSocket (n - bufLen)
      return (BS.empty,b <> buf)

recvL :: BufferedSocket -> Int -> IO BL.ByteString
recvL BufferedSocket{..} n = modifyMVar bBuffer $ \buf -> do
  let bufLen = BS.length buf
  if n <= bufLen
    then return . fmap BL.fromStrict . swap . BS.splitAt n $ buf
    else do
      b <- SL.recv bSocket (fromIntegral (n - bufLen))
      return (BS.empty,b <> BL.fromStrict buf)

type BufferdIO b a = (Int -> IO b) -- ^ recv method
                  -> b             -- ^ initial
                  -> IO (b,a)      -- ^ (unused,result)

withBufS :: BufferedSocket -> BufferdIO BS.ByteString a -> IO a
withBufS BufferedSocket{..} f = modifyMVar bBuffer $ f (SS.recv bSocket)

withBufL :: BufferedSocket -> BufferdIO BL.ByteString a -> IO a
withBufL BufferedSocket{..} f = modifyMVar bBuffer $
  fmap (first BL.toStrict) . f (SL.recv bSocket . fromIntegral) . BL.fromStrict

sendSAll :: BufferedSocket -> BS.ByteString -> IO ()
sendSAll BufferedSocket{bSocket=s} = SS.sendAll s

sendLAll :: BufferedSocket -> BL.ByteString -> IO ()
sendLAll BufferedSocket{bSocket=s} = SL.sendAll s
