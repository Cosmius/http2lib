{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module HTTP2Lib.Frame
  ( HTTP2Frame(..)
  , decodeFrame
  , FramePayload(..)
  , decodeFrameHeader
  , module F
  ) where

import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.ByteString
import           Data.Bits
import           Data.Word

import           HTTP2Lib.Frame.Data        as F
import           HTTP2Lib.Frame.Headers     as F
import           HTTP2Lib.Frame.Internal    as F
import           HTTP2Lib.Frame.Priority    as F
import           HTTP2Lib.Frame.Unknown     as F
import           HTTP2Lib.Utils

data HTTP2Frame = HTTP2Frame
  { frameHeader  :: FrameHeader
  , framePayload :: FramePayload
  }
  deriving (Show)

data FramePayload
  = FData ByteString
  | FHeaders HeadersPayload
  | FPriority PriorityData
  | FUnknown UnknownPayload
  deriving (Show)

decodeFrameHeader :: Parser (FrameHeader,Word8)
decodeFrameHeader = do
  lenAndType <- pWord32BE
  frameFlags <- P.anyWord8
  streamId' <- pWord32BE
  let frameLen      = fromIntegral $ shiftR lenAndType 8
      ft            = fromIntegral $ lenAndType .&. 0xff
      frameStreamId = clearBit streamId' 31
  return (FrameHeader{..}, ft)

decodeFrame :: Parser HTTP2Frame
decodeFrame = do
  (header,ft) <- decodeFrameHeader
  HTTP2Frame header <$> case ft of
    0x0 -> FData     <$> decodeDataPayload header
    0x1 -> FHeaders  <$> decodeHeadersPayload header
    0x2 -> FPriority <$> decodePriorityPayload header
    _   -> FUnknown  <$> decodeUnknownPayload ft header
