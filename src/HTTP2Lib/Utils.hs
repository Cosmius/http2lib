module HTTP2Lib.Utils
  ( pWord32BE
  , assert
  ) where

import           Control.Monad.Fail         (MonadFail)
import           Control.Monad.Fail         as M
import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.Bits
import           Data.ByteString
import           Data.Word

pWord32BE :: Parser Word32
pWord32BE = pWordNBE 4

pWordNBE :: (Num a, Bits a) => Int -> Parser a
pWordNBE n = foldWordNBE <$> P.take n

foldWordNBE :: (Num a, Bits a) => ByteString -> a
foldWordNBE = foldl' (\acc n -> shiftL acc 8 .|. fromIntegral n) zeroBits

assert :: MonadFail m => Bool -> String -> m ()
assert True  = const $ return ()
assert False = M.fail
