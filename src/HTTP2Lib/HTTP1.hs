{-# LANGUAGE OverloadedStrings #-}
module HTTP2Lib.HTTP1
 ( RequestHeader(..), ResponseHeader(..)
 , encodeRequestHeader
 , decodeResponseHeader
 ) where

import           Control.Applicative
import           Data.Attoparsec.ByteString       (Parser)
import qualified Data.Attoparsec.ByteString.Char8 as P
import           Data.ByteString
import           Data.ByteString.Builder          as Builder
import           Data.Semigroup

import           HTTP2Lib.Internal
import           HTTP2Lib.Utils

type Headers = [HeaderEntry]

data RequestHeader = RequestHeader
  { requestMethod  :: ByteString
  , requestPath    :: ByteString
  , requestHeaders :: Headers
  }
  deriving (Show)

data ResponseHeader = ResponseHeader
  { responseStatus  :: Int
  , responseReason  :: ByteString
  , responseHeaders :: Headers
  }
  deriving (Show)

encodeHeaders :: Headers -> Builder
encodeHeaders = foldMap $ \(name,val) -> byteString name
                                      <> ": "
                                      <> byteString val
                                      <> "\r\n"

decodeHeaders :: Parser Headers
decodeHeaders = many $ do
  firstChar <- P.peekChar'
  assert (firstChar /= '\r') "Unexpected CR"
  name <- P.takeTill (== ':')
  _ <- P.char ':'
  P.skipWhile (== ' ')
  val <- P.takeTill (== '\r')
  _ <- "\r\n"
  return (name,val)

encodeRequestHeader :: RequestHeader -> Builder
encodeRequestHeader (RequestHeader m p hs) = byteString m
                                          <> Builder.char8 ' '
                                          <> byteString p
                                          <> " HTTP/1.1\r\n"
                                          <> encodeHeaders hs
                                          <> "\r\n"

decodeResponseHeader :: Parser ResponseHeader
decodeResponseHeader = do
  _ <- "HTTP/1.1"
  P.skipWhile (== ' ')
  status <- P.decimal
  P.skipWhile (== ' ')
  reason <- P.takeTill (== '\r')
  _ <- "\r\n"
  headers <- decodeHeaders
  _ <- "\r\n"
  return (ResponseHeader status reason headers)
