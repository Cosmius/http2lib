module HTTP2Lib.Internal
  ( HeaderEntry
  ) where

import           Data.ByteString

type HeaderEntry = (ByteString,ByteString)
