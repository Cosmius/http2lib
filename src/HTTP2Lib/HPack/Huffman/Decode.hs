{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RankNTypes        #-}
{-# LANGUAGE RecordWildCards   #-}
module HTTP2Lib.HPack.Huffman.Decode
  ( decodeHuffman
  ) where

import           Control.Monad.ST
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Reader
import           Data.Bits
import           Data.ByteString
import           Data.STRef
import           Data.Word

import           HTTP2Lib.HPack.Huffman.Internal

data DecodeState s = DecodeState
  { stateBuf    :: STRef s Word8      -- current byte
  , statePos    :: STRef s Word8      -- current position
  , stateInput  :: STRef s ByteString -- remaining bytestring
  , stateOutput :: STRef s ByteString -- output
  }

type HuffmanDecode a = forall s. ReaderT (DecodeState s) (ST s) a

decodeHuffman :: ByteString -> ByteString
decodeHuffman input = runST $ do
  s <- newDecodeState input
  runReaderT decodeHuffman' s
  readSTRef $ stateOutput s

decodeHuffman' :: HuffmanDecode ()
decodeHuffman' = do
  mv <- nextHuffman decodeHuffmanTable
  case mv of
    Nothing -> return ()
    Just x  -> do
      DecodeState{..} <- ask
      lift $ modifySTRef' stateOutput (`snoc` x)
      decodeHuffman'

nextHuffman :: HuffmanTree (Maybe Word8) -> HuffmanDecode (Maybe Word8)
nextHuffman (HuffmanNode x) = return x
nextHuffman (HuffmanTree l r) = do
  b <- nextBit
  case b of
    Just 0 -> nextHuffman r
    Just _ -> nextHuffman l
    _      -> return Nothing

nextBit :: HuffmanDecode (Maybe Word8)
nextBit = do
  DecodeState{..} <- ask
  lift $ do
    pos <- readSTRef statePos
    if pos /= 0
      then do
        writeSTRef statePos $ shiftR pos 1
        buf <- readSTRef stateBuf
        return . Just $ pos .&. buf
      else do
        writeSTRef statePos 0x40
        input <- readSTRef stateInput
        case uncons input of
          Just (buf,input') -> do
            writeSTRef stateBuf buf
            writeSTRef stateInput input'
            return . Just $ 0x80 .&. buf
          Nothing -> return Nothing

newDecodeState :: ByteString -> ST s (DecodeState s)
newDecodeState input = do
  stateBuf <- newSTRef 0x0
  statePos <- newSTRef 0x0
  stateInput <- newSTRef input
  stateOutput <- newSTRef ""
  return DecodeState{..}
