{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RankNTypes        #-}
{-# LANGUAGE RecordWildCards   #-}
module HTTP2Lib.HPack.Huffman.Encode
  ( encodeHuffman
  ) where

import           Data.Bits
import           Data.ByteString                 (ByteString)
import qualified Data.ByteString                 as B
import           Data.Semigroup
import qualified Data.Sequence                   as S
import           Data.Word
import           HTTP2Lib.HPack.Huffman.Internal

encodeHuffman :: ByteString -> ByteString
encodeHuffman input =
  let (fws,fr)    = B.foldl f (mempty,HBits 0 0) input
      f (ws, r) w =
        let code     = S.index encodeHuffmanTable $ fromIntegral w
            (ws',r') = appendHBits r code
        in (ws <> ws', r')
  in B.pack $ fws <> finalizeHBits fr

appendHBits :: HBits -> HBits -> ([Word8],HBits)
appendHBits (HBits tw tl) (HBits aw al) = clearHBits (HBits nw nl)
  where
  nw = shiftL tw al .|. aw
  nl = tl + al

clearHBits :: HBits -> ([Word8],HBits)
clearHBits (HBits w len)
  | len < 8   = ([],HBits w len)
  | otherwise =
    let d = len - 8
        b = fromIntegral $ shiftR w d
        (bs,r') = clearHBits (HBits w d)
    in (b:bs,r')

finalizeHBits :: HBits -> [Word8]
finalizeHBits (HBits _ 0) = []
finalizeHBits x = [head . fst $ appendHBits x (S.index encodeHuffmanTable 256)]
