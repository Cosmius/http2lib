{-# OPTIONS_GHC -Wno-missing-fields #-}
{-# LANGUAGE DeriveLift        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TemplateHaskell   #-}
module HTTP2Lib.HPack.Huffman.TH
  ( HuffmanTree(..), HBits(..)
  , mkHuffmanTable
  ) where

import           Control.Applicative
import           Control.Monad
import           Data.Attoparsec.ByteString       (Parser)
import           Data.Attoparsec.ByteString.Char8 as P
import           Data.Bits
import           Data.ByteString.Char8            (pack)
import           Data.Either
import           Data.Sequence                    (Seq)
import qualified Data.Sequence                    as S
import           Data.Word
import           Language.Haskell.TH
import           Language.Haskell.TH.Quote
import           Language.Haskell.TH.Syntax

data HuffmanTree a
  = HuffmanNode a
  -- | Go left when bit 1, right when bit 0
  | HuffmanTree (HuffmanTree a) (HuffmanTree a)
  deriving (Show, Lift)

instance Functor HuffmanTree where
  fmap f (HuffmanNode x)   = HuffmanNode (f x)
  fmap f (HuffmanTree l r) = HuffmanTree (fmap f l) (fmap f r)

mkHuffmanTable :: QuasiQuoter
mkHuffmanTable = QuasiQuoter {quoteDec = mkHuffmanTable'}

mkHuffmanTable' :: String -> Q [Dec]
mkHuffmanTable' input = do
  let hItems = rights $ P.parseOnly decodeLine . pack <$> lines input
      lookupTree = foldl insertItem emptyHuffman hItems
      -- I'm sure hItems is ordered
      encodeTable = snd <$> hItems
  if length encodeTable /= 257
    then fail "Bad data"
    else return ()
  [d|
    decodeHuffmanTable :: HuffmanTree (Maybe Word8)
    decodeHuffmanTable = $(lift lookupTree)
    encodeHuffmanTable :: Seq HBits
    encodeHuffmanTable = S.fromList $(lift encodeTable)
    |]

insertItem :: HuffmanTree (Maybe Word8)
           -> (Word16, HBits)
           -> HuffmanTree (Maybe Word8)
insertItem t (w,bs)
  | w >= 256  = t
  | otherwise = insertTree bs (fromIntegral w) t

insertTree :: HBits -> a -> HuffmanTree (Maybe a) -> HuffmanTree (Maybe a)
insertTree bs x (HuffmanNode Nothing)
  | noBits bs = HuffmanNode (Just x)
  | testHBits bs = HuffmanTree (insertTree (shiftHBits bs) x emptyHuffman) emptyHuffman
  | otherwise = HuffmanTree emptyHuffman (insertTree (shiftHBits bs) x emptyHuffman)
insertTree bs x (HuffmanTree l r)
  | noBits bs = error "Cannot insert into a branch"
  | testHBits bs = HuffmanTree (insertTree (shiftHBits bs) x l) r
  | otherwise = HuffmanTree l (insertTree (shiftHBits bs) x r)
insertTree _ _ _ = error "Insert on a non-empty node"

emptyHuffman :: HuffmanTree (Maybe a)
emptyHuffman = HuffmanNode Nothing

decodeLine :: Parser (Word16, HBits)
decodeLine = do
  skipSpace
  _ <- optional $ eos <|> qChar
  skipSpace
  _ <- P.char8 '('
  skipSpace
  byte <- P.decimal
  _ <- P.char8 ')'
  skipSpace
  _ <- some $ P.char8 '|' *> P.takeWhile (P.inClass "01")
  skipSpace
  encoded <- P.hexadecimal
  skipSpace
  _ <- P.char8 '['
  skipSpace
  len <- P.decimal
  _ <- P.char8 ']'
  return (byte,HBits encoded len)
  where
  eos = void "EOS"
  qChar = void $ P.char8 '\'' *> P.anyChar <* P.char8 '\''

-- * HBits

-- the longest bit sequence is 30-bit long, 38-bit is required to store a byte
-- and the buffer Word64 should be sufficient for 2 ch
--                 data   len
data HBits = HBits Word64 Int deriving (Show, Lift)

testHBits :: HBits -> Bool
testHBits (HBits w len) = testBit w (len - 1)

noBits :: HBits -> Bool
noBits (HBits _ len) = len <= 0

shiftHBits :: HBits -> HBits
shiftHBits (HBits w len) = HBits w $ len - 1
