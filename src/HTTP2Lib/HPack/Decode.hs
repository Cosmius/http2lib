{-# LANGUAGE OverloadedStrings #-}
module HTTP2Lib.HPack.Decode
  ( parseHeaderHPacked
  ) where

import           Control.Applicative
import           Control.Arrow
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.State
import           Data.Attoparsec.ByteString (Parser)
import qualified Data.Attoparsec.ByteString as P
import           Data.Bits
import           Data.ByteString            (ByteString)
import qualified Data.ByteString            as B
import           Data.Maybe
import           Data.Word

import           HTTP2Lib.HPack.Huffman
import           HTTP2Lib.HPack.Internal
import           HTTP2Lib.Internal

parseHeaderHPacked :: SizedDynamicTable -> ByteString
                   -> ([HeaderEntry],SizedDynamicTable)
parseHeaderHPacked t b =
  first catMaybes $ case P.parseOnly (parser <* P.endOfInput) b of
    Left err -> error err
    Right r  -> r
  where
  parser = runStateT parseHeaderHPacked' t

type HPackDecode = StateT SizedDynamicTable Parser

parseHeaderHPacked' :: HPackDecode [Maybe HeaderEntry]
parseHeaderHPacked' = many $ do
  rawHeader <- lift pHeaderField
  (size,t) <- get
  let putAndReturn Incremental h =
        put (size,pushDynamicTable size h t) >> return (Just h)
      putAndReturn _ h = return (Just h)
  case rawHeader of
    IndexedField i -> case readHeaderTable i t of
      Just h  -> return . Just $ h
      Nothing -> error "Header lookup failed"
    DynamicTableUpdate n -> put (n,truncateDynamicTable n t) >> return Nothing
    NameIndexedField p i val -> do
      name <- case readHeaderTable i t of
        Just (name,_) -> return name
        Nothing       -> error "Header lookup failed"
      putAndReturn p (name,val)
    LiteralField p name val -> putAndReturn p (name,val)

pHeaderField :: Parser HeaderField
pHeaderField = P.anyWord8 >>= \w -> case w of
  _ | testBit w 7 ->
      IndexedField <$> pIntPlus 0x7f w
    | w .&. 0xe0 == 0x20 ->
      return . DynamicTableUpdate . fromIntegral $ w .&. 0x1f
    | w == 0x40 || w == 0x10 || w == 0 ->
      let policy = case w of
            0x40 -> Incremental
            0x10 -> NeverIndex
            _    -> NoIndex
      in LiteralField policy <$> pString <*> pString
    | otherwise ->
      let (policy,mask) = case w of
            _ | testBit w 6 -> (Incremental,0x3f)
              | testBit w 4 -> (NeverIndex,0x0f)
              | otherwise   -> (NoIndex,0x0f)
      in NameIndexedField policy <$> pIntPlus mask w <*> pString

pIntPlus :: Word8 -> Word8 -> Parser Int
pIntPlus mask firstByte =
  let masked = firstByte .&. mask
  in if masked < mask
    then return (fromIntegral masked)
    else do
      bs <- P.takeWhile $ \w -> testBit w 7
      msb <- fromIntegral <$> P.anyWord8
      let f w acc = acc * 128 + fromIntegral w - 128
      return $ B.foldr' f msb bs + fromIntegral masked

pString :: Parser ByteString
pString = do
  w <- P.anyWord8
  len <- pIntPlus 0x7f w
  (if testBit w 7 then decodeHuffman else id) <$> P.take len
