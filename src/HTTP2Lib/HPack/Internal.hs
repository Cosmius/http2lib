module HTTP2Lib.HPack.Internal
  ( HeaderField(..), IndexPolicy(..)
  , SizedDynamicTable, DynamicTable
  , emptyDynamicTable, truncateDynamicTable
  , pushDynamicTable, readHeaderTable
  ) where

import           Data.ByteString            (ByteString)
import qualified Data.ByteString            as B
import           Data.Semigroup
import           Data.Sequence              (Seq, ViewL (..), (|>))
import qualified Data.Sequence              as S

import           HTTP2Lib.HPack.StaticTable
import           HTTP2Lib.Internal

data IndexPolicy = Incremental | NoIndex | NeverIndex deriving (Show)

data HeaderField
  = IndexedField Int
  | NameIndexedField IndexPolicy Int ByteString
  | LiteralField IndexPolicy ByteString ByteString
  | DynamicTableUpdate Int
  deriving (Show)

type SizedDynamicTable = (Int,DynamicTable)
type DynamicTable = Seq HeaderEntry

emptyDynamicTable :: DynamicTable
emptyDynamicTable = S.empty

dynamicTableSize :: DynamicTable -> Int
dynamicTableSize =
  getSum . foldMap (\(a,b) -> Sum $ B.length a + B.length b + 32)

pushDynamicTable :: Int -> HeaderEntry -> DynamicTable -> DynamicTable
pushDynamicTable n h t = truncateDynamicTable n $ t |> h

truncateDynamicTable :: Int -> DynamicTable -> DynamicTable
truncateDynamicTable n t
  | dynamicTableSize t <= n = t
  | otherwise = let (_ :< t') = S.viewl t in truncateDynamicTable n t'

readHeaderTable :: Int -> DynamicTable -> Maybe HeaderEntry
readHeaderTable i t
  | i <= maxStaticIndex = Just $ S.index staticTable (i - 1)
  | otherwise           = let i' = i - maxStaticIndex - 1
                          in if i' < length t
                            then Just $ S.index t i'
                            else Nothing
