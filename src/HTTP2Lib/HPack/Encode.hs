{-# LANGUAGE OverloadedStrings #-}
module HTTP2Lib.HPack.Encode
  ( encodeHPacked
  ) where

import           Control.Applicative
import           Control.Monad.Trans.State
import           Data.Bits
import           Data.ByteString           (ByteString)
import qualified Data.ByteString           as B
import           Data.List
import           Data.Semigroup
import           Data.Word

import           HTTP2Lib.HPack.Internal
import           HTTP2Lib.Internal

encodeHPacked :: SizedDynamicTable -> [HeaderEntry]
              -> (ByteString,SizedDynamicTable)
encodeHPacked t hs = runState (foldA encodeHeaderEntry hs) t

encodeHeaderEntry :: HeaderEntry -> State SizedDynamicTable ByteString
-- TODO: Index
encodeHeaderEntry (name,val) = return $
  B.cons 0x0 $ bString name <> bString val

foldA :: (Monoid b, Applicative f, Foldable t) => (a -> f b) -> t a -> f b
foldA f = foldr (liftA2 mappend . f) (pure mempty)

bIntPlus :: Word8 -> Int -> Word8 -> ByteString
bIntPlus mask val left = B.cons firstByte other
  where
  isOneByte = val < fromIntegral mask
  firstByte = left .|. if isOneByte then fromIntegral val .&. mask else mask
  t         = val - fromIntegral mask
  other     = if isOneByte then "" else B.pack . reverse . fHead $ unfoldr f t
  fHead []     = []
  fHead (x:xs) = clearBit x 7 : xs
  f :: Int -> Maybe (Word8,Int)
  f 0 = Nothing
  f v = let (q,r) = quotRem v 128 in Just (setBit (fromIntegral r) 7,q)

bString :: ByteString -> ByteString
-- TODO: Huffman encoding
bString b = (bIntPlus 0x7f (B.length b) 0x0) <> b
