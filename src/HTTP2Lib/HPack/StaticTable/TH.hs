{-# OPTIONS_GHC -Wno-missing-fields #-}
module HTTP2Lib.HPack.StaticTable.TH
  ( mkStaticTable
  ) where

import           Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString.Char8            as BC
import           Data.Either
import           Language.Haskell.TH
import           Language.Haskell.TH.Quote
import           Language.Haskell.TH.Syntax

mkStaticTable :: QuasiQuoter
mkStaticTable = QuasiQuoter {quoteExp = mkStaticTable'}

mkStaticTable' :: String -> Q Exp -- [(String,String)]
mkStaticTable' = lift . rights . fmap processLine . BC.lines . BC.pack

processLine :: BC.ByteString -> Either String (String, String)
processLine = parseOnly $ do
  skipSpace
  _ <- char '|'
  skipSpace
  _ <- decimal :: Parser Int
  skipSpace
  _ <- char '|'
  skipSpace
  name <- BC.unpack <$> takeTill isSpace
  skipSpace
  _ <- char '|'
  skipSpace
  val' <- BC.unpack <$> takeTill (== '|')
  let val = reverse . dropWhile isSpace . reverse $ val'
  return (name,val)
