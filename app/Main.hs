{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Main (main) where

import           Data.Attoparsec.ByteString
import           Data.ByteString            (ByteString)
import           Data.ByteString.Builder    (toLazyByteString)
import           Data.Semigroup
import           Data.Word

import           HTTP2Lib.BufferedSocket    as BufSock
import           HTTP2Lib.Frame
import           HTTP2Lib.HTTP1
import           HTTP2Lib.HPack

main :: IO ()
main = do
  putStrLn "creating socket"
  s <- BufSock.new
  -- nghttp2 is a website that can be used to test http2 client
  putStrLn "connecting to nghttp2.org:80"
  BufSock.connect s "nghttp2.org" "80"
  let req = RequestHeader "GET" "/httpbin/get"
        [ ("Host","nghttp2.org")
        , ("Connection","Upgrade")
        , ("Upgrade","h2c")
        , ("HTTP2-Settings","")
        ]
  sendHttp1RequestHeader s req
  resp <- recvHttp1RequestHeader s
  case resp of
    ResponseHeader{responseStatus=101} -> do
      putStrLn "Got 101 Response"
      print resp
    _ -> fail $ "expected 101, but got " <> show resp
  recvFrame s >>= print -- settings frame, ignore it here
  (isES,encodedHeader) <- collectHeaders s 1
  putStrLn "received encoded headers"
  print encodedHeader
  let dt = (4096,mempty)
      (headers,_) = parseHeaderHPacked dt encodedHeader
  putStrLn "decoded headers"
  foldr (\x acc -> print x >> acc) (return ()) headers
  -- ... I hate HPack
  body <- if isES then return mempty else collectData s 1
  putStrLn "received body"
  print body
  recvAndPrintAllFrames s
  BufSock.close s

sendHttp1RequestHeader :: BufferedSocket -> RequestHeader -> IO ()
sendHttp1RequestHeader s = sendLAll s . toLazyByteString . encodeRequestHeader

recvHttp1RequestHeader :: BufferedSocket -> IO ResponseHeader
recvHttp1RequestHeader s = withBufS s $ \recv buf -> do
  result <- parseWith (recv 4096) decodeResponseHeader buf
  case result of
    Done i r -> return (i,r)
    _        -> fail $ "Bad result: " <> show result

recvFrame :: BufferedSocket -> IO HTTP2Frame
recvFrame s = withBufS s $ \recv buf -> do
    result <- parseWith (recv 4096) decodeFrame buf
    case result of
      Done i r -> return (i,r)
      _        -> fail $ "Bad result: " <> show result

recvAndPrintAllFrames :: BufferedSocket -> IO ()
recvAndPrintAllFrames s = do
  frame <- recvFrame s
  putStrLn "Got Frame:"
  print frame
  recvAndPrintAllFrames s

-- the result should be followed by 0 or more Continue frames, but...
-- also, not handle priority either
-- returns (isEndStream,headerFrags)
collectHeaders :: BufferedSocket -> Word32 -> IO (Bool,ByteString)
collectHeaders s sid = do
  HTTP2Frame{frameHeader=FrameHeader{..},framePayload=payload} <- recvFrame s
  case payload of
    FHeaders HeadersPayload{..} -> do
      let isES = frameFlags `hasFlag` endStream
          isEH = frameFlags `hasFlag` endHeaders
      if not isEH || frameStreamId /= sid
        then fail "Cannot handle headers in more than one frame"
        else return (isES,headersFrag)
    _ -> collectHeaders s sid

collectData :: BufferedSocket -> Word32 -> IO ByteString
collectData s sid = do
  HTTP2Frame{frameHeader=FrameHeader{..},framePayload=payload} <- recvFrame s
  (prepend,shouldStop) <- case payload of
    FData frag -> if frameStreamId /= sid
      then fail "no concurrent supported"
      else return ((frag <>), frameFlags `hasFlag` endStream)
    _ -> return (id,False)
  prepend <$> if shouldStop then return mempty else collectData s sid
