{-# LANGUAGE OverloadedStrings #-}
import Test.Hspec
import Data.ByteString

import HTTP2Lib.HPack

main :: IO ()
main = hspec $ do
  describe "decode huffman" $ do
    it "decode empty string" $ do
      decodeHuffman (pack
        [ 0xff, 0xff, 0xff, 0xfc
        ]) `shouldBe` ""
    it "decode abc string" $ do
      -- 00011 100011 00100 11111111|11111111|11111111|111111
      --  |
      --  v
      -- 00011100 01100100  11111111 11111111 11111111 11111100
      decodeHuffman (pack
        [ 0x1c,   0x64,     0xff,     0xff,     0xff,   0xfc
        ]) `shouldBe` "abc"
  describe "encode huffman" $ do
    it "encode empty string" $ do
      encodeHuffman "" `shouldBe` ""
    it "decode abc string" $ do
      -- 00011 100011 00100 11111111|11111111|11111111|111111
      --  |
      --  v
      -- 00011100 01100100  11111111 11111111 11111111 11111100
      encodeHuffman "abc" `shouldBe` pack [0x1c, 0x64]
